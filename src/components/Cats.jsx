import React, { useState, useEffect } from 'react'
import axios from 'axios'

const Cats = () => {
    const [cats, setCats] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const fetchCats = async () => {
            try {
                const res = await axios.get('https://api.thecatapi.com/v1/images/search?limit=10');
                setCats(res.data);
                setLoading(false);
            } catch (error) {
                console.log("Error al obtener los gaticos", error)
            }
        }
        fetchCats();
    }, [loading]);
  return (
    <div>
        <h2>Gatitos: </h2>
        <ul>
        {cats.map((cat) => (
            <img src={cat.url}></img>
        ))}
      </ul>
    </div>
    
  )
}

export default Cats