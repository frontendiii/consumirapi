import React from 'react'
import './App.css'
import Cats from './components/Cats'

function App() {


  return (
    <React.Fragment>
      <Cats />
    </React.Fragment>
  )
}

export default App
